package pl.sda.model.entity;

public enum DocumentSource {
    FORM_ANALYZER,
    MIGRATION
}
