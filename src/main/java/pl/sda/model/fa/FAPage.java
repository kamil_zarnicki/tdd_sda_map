package pl.sda.model.fa;

import java.util.List;

public class FAPage {
    private String number;
    private List<FAZone> zones;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<FAZone> getZones() {
        return zones;
    }

    public void setZones(List<FAZone> zones) {
        this.zones = zones;
    }
}
