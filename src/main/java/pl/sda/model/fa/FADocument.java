package pl.sda.model.fa;

import java.util.List;

public class FADocument {
    private String id;
    private String file;
    private String folder;
    private String template;
    private String status;
    private List<FAPage> pages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FAPage> getPages() {
        return pages;
    }

    public void setPages(List<FAPage> pages) {
        this.pages = pages;
    }
}
